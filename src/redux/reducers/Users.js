import { Types } from '../../consts'

const initUsers = {
  users: []
}

export const usersReducer = (state = initUsers, action) => {
  if (action.type === Types.USERS) {
    return {
      ...state,
      users: action.value
    }
  }

  return state
}