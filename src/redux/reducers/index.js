import { combineReducers } from 'redux'
import { usersReducer } from './Users'
import { globalReducer } from './Global'

const reducer = combineReducers({
  usersReducer,
  globalReducer
})

export default reducer