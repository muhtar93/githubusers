import { Networks, Types } from '../../consts'
import axios from 'axios'
import { setLoading } from './Global'

export const getAllUsers = (user) => (dispatch) => {
  dispatch(setLoading(true))
  axios.get(`${Networks.BASE_URL}search/users?q=${user}&per_page=10`)
    .then(res => {
      console.log('res', res.data)
      dispatch({ type: Types.USERS, value: res.data })
      dispatch(setLoading(false))
    })
    .catch(err => {
      console.log('error', err)
      dispatch(setLoading(false))
    })
}