import React, {useEffect} from 'react'
import {TextInput, View, FlatList, StyleSheet, SafeAreaView} from 'react-native'
import { Colors, Fonts } from '../../consts'
import {useDispatch, useSelector} from 'react-redux'
import {getAllUsers} from '../../redux/actions'
import { TeamItem } from '../../components'

const Users = () => {
  const dispatch = useDispatch()
  const { users } = useSelector(state => state.usersReducer)

  useEffect(() => {
    dispatch(getAllUsers('a'))
  }, [])

  const onTypeSearch = (search) => {
    dispatch(getAllUsers(search))
  }

  return (
    <>
    <SafeAreaView style={styles.safeAreaTop} />
    <View style={styles.container}>
    <TextInput 
        style={styles.input}
        placeholder='type username'
        onChangeText={(value) => onTypeSearch(value)}
      />
      <FlatList
          data={users.items}
          renderItem={({ item, index }) =>
            <TeamItem 
              name={item.login}
              source={item.avatar_url}
            />
          }
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
        />
    </View>
    </>
  )
}

const styles = StyleSheet.create({
  safeAreaTop: {
    backgroundColor: Colors.PRIMARY,
    flex: 0
  },
  container: {
    flex: 1
  },
  input: {
    height: 50,
    margin: 12,
    fontFamily: Fonts.REGULAR,
    width: null,
    borderWidth: 1,
    padding: 16,
    borderRadius: 24,
    borderColor: '#CECECE',
    elevation: 10,
    shadowRadius: 4.65,
    shadowOffset: {
      height: 1,
      width: 1
    }
  }
})

export default Users