import 'react-native-gesture-handler'
import * as React from 'react'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import Users from '../screens/users/Users'

const Stack = createStackNavigator()

const Main = () => {
  const options = {
    gestureEnabled: true,
    ...TransitionPresets.SlideFromRightIOS
  }

  return (
    <Stack.Navigator
      initialRouteName='Users'
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white'
        }
      }}>
      <Stack.Screen name='Users' component={Users} options={options} />
    </Stack.Navigator>
  )
}

export default Main