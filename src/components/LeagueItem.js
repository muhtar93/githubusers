import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { Gap } from '.'
import { Colors, Fonts } from '../consts'

const LeagueItem = ({ name, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Gap height={8} />
      <Text style={styles.textSubTitle}>{name}</Text>
      <Gap height={8} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingHorizontal: 8
  },
  textSubTitle: {
    color: Colors.SURFACE,
    fontFamily: Fonts.BOLD,
    fontSize: 16
  }
})

export default LeagueItem