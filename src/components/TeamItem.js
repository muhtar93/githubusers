import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Gap } from '.'
import { Colors, Fonts } from '../consts'

const TeamItem = ({ name, onPress, source }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Gap height={8} />
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Image 
          source={{uri: source}}
          style={styles.icon}
        />
        <Text style={styles.textSubTitle}>{name}</Text>
      </View>
      <Gap height={8} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingHorizontal: 8
  },
  textSubTitle: {
    color: Colors.SURFACE,
    fontFamily: Fonts.BOLD,
    fontSize: 16,
    marginLeft: 16
  },
  icon:{
    width: 60,
    height: 60
  }
})

export default TeamItem